<?php

namespace HomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('main/index.html.twig');
    }

	public function blogAction()
    {
        return $this->render('blog/index.html.twig');
    }
}
